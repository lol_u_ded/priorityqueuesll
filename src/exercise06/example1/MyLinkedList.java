package exercise06.example1;

public class MyLinkedList implements MyList {
	private MyListNode head;
	private MyListNode tail;
	private int size;
		
	/**
	 * Constructor.
	 * Initializes List.
	 */
	public MyLinkedList() {
		head = null;
		tail = null;
		head = tail;
		size = 0;
	}
	
	@Override
	public exercise06.example1.MyListNode getHead() {
		return head;
	}
	
	@Override
	public int size() {
		return size;
	}

	@Override
	public void addFirst(Integer elem) throws IllegalArgumentException {
		MyListNode node = new MyListNode(elem);
		if(head == null){
			head = node;
			head.setNext(tail);
		}else{
			node.setNext(head);
			head = node;
		}
		size++;
	}

	@Override
	public void addLast(Integer elem) throws IllegalArgumentException {
		MyListNode node = new MyListNode(elem);
		if(tail != null){
			tail.setNext(node);
			tail = node;
		}else{
			if(head != null){
				MyListNode curr = head;
				while(hasNext(curr)){
					curr = curr.getNext();
				}
				curr.setNext(node);
				tail = node;
			}else{
				addFirst(elem);
			}
		}
	}

	@Override
	public void addSorted(Integer elem) throws IllegalArgumentException {
		if( head == null){
			addFirst(elem);
		}else {
			MyListNode curr = head;
			MyListNode prev = null;

			while (curr != null && elem.compareTo(curr.getElement()) > 0) {
				prev = curr;
				curr = curr.getNext();
			}

			// Sort into List

			// Case1: add as head
			if (curr == head) {
				addFirst(elem);

			// Case2: add as tail
			} else if (curr == null) {
				addLast(elem);

			//Case3: add somewhere in the middle of the list
			} else {
				MyListNode node = new MyListNode(elem);
				prev.setNext(node);
				node.setNext(curr);
				size++;
			}
		}
	}

			
	@Override
	public void clear() {
		head = null;
		tail = null;
		head = tail;
		size = 0;
	}

	@Override
	public Integer removeFirst() {
		if(head != null){
			Integer integer = head.getElement();
			head = head.getNext();
			size--;
			return integer;
		}
		return null;
	}

	@Override
	public Integer getFirst() {
		return head.getElement();
	}

	@Override
	public boolean contains(Integer elem) throws IllegalArgumentException {
		if(elem == null || head == null) {
			throw new IllegalArgumentException();
		}
		MyListNode curr = head;
		while(curr != tail){
			if(curr.getElement().compareTo(elem) == 0){
				return true;
			}else{
				curr = curr.getNext();
			}
		}
		return false;
	}

	@Override
	public Integer[] toArray() {
		if(size > 0) {
			MyListNode curr = head;
			Integer[] arr = new Integer[size];
			for(int i = 0; i < size-1; i++){
				arr[i] = curr.getElement();
				curr = curr.getNext();
			}
			return arr;
		}else{
			return null;
		}
	}
	
	public String toString() {
		Integer[] arr = toArray();
		StringBuilder stringBuilder = new StringBuilder();
//		stringBuilder.append("Current ListElements: \n");
		for(Integer elem : arr){
			stringBuilder
					.append("[")
					.append(elem)
					.append("]");
		}
		return stringBuilder.toString();
	}

	private boolean hasNext(MyListNode node) {
		return node.getNext() != null;
	}

}
