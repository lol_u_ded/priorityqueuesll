package exercise06.example1;

public interface MyList {
	/**
	 * This method is for testing purpose. It returns the head of the list.
	 * 
	 * @return the head of the list.
	 */
	MyListNode getHead();
	
	/**
	 * 
	 * @return Returns the number of elements in the list (in O(1))
	 */
	int size();					
	
	/**
	 * This method inserts an element at the beginning of the list.
	 * 
	 * @param elem The data to be inserted into the list.
	 * @throws IllegalArgumentException if the passed object is null.
	 */
	void addFirst(Integer elem) throws IllegalArgumentException;
	
	/**
	 * This method inserts an element at the end of the list.
	 * 
	 * @param elem The data to be inserted into the list.
	 * @throws IllegalArgumentException if the passed object is null.
	 */
	void addLast(Integer elem) throws IllegalArgumentException;
	
	/**
	 * This method inserts an element at the correct position into an ascending sorted list.
	 * 
	 * @param elem The data to be inserted into the list.
	 * @throws IllegalArgumentException if the passed object is null.
	 */
	void addSorted(Integer elem) throws IllegalArgumentException;
	
	/**
	 * Clears the list by removing all elements (in O(1))
	 */
	void clear();
	
	/**
	 * This method returns the data of the first list element and removes it from the list.
	 * If the list is empty, return null.
	 * @return Data of the first list element, or null if list is empty.
	 */
	Integer removeFirst();
	
	/**
	 * This method returns the data of the first element of the list without removing it.
	 * If the list is empty, return null.
	 * @return Data of the first list element, or null if list is empty.
	 */
	Integer getFirst();
	
	/**
	 * This method searches the given element in the list and returns true if found, false otherwise.
	 * 
	 * @param elem Element to be search in the list.
	 * @return true if elem is found, false otherwise.
	 * @throws IllegalArgumentException if the given elem is null.
	 */
	boolean contains(Integer elem) throws IllegalArgumentException;
	
	/**
	 * This method returns a String-representation of the list, where each element is surrounded by square brackets.
	 * e.g.: For a list with elements 1->5->7->20 this method would return: "[1][5][7][20]"
	 * @return String list represented as a String.
	 */
	String toString();
	
	/**
	 * This method returns the list starting from head to tail as array.
	 * 
	 * @return list represented as array.
	 */
	Integer[] toArray();
}
