package exercise06.example1;

public class MyListNode {

	private Integer element;
	private MyListNode next;
	
	public MyListNode() {
		this.element = null;
		this.next = null;
	}
	
	public MyListNode(Integer element) {
		this.element = element;
		this.next = null;
	}
	
	public Integer getElement() 			{ return this.element; }
	public void setElement(Integer element)	{ this.element = element; }
	public MyListNode getNext() 			{ return this.next; }
	public void setNext(MyListNode next)	{ this.next = next; }
}

