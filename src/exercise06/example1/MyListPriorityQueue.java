package exercise06.example1;

import java.util.NoSuchElementException;

public class MyListPriorityQueue implements MyPriorityQueue {

	protected MyLinkedList list;
	
	public MyListPriorityQueue() {
		this.list = new MyLinkedList();
	}

	@Override
	public int size() {
		return list.size();
	}
	
	@Override
	public boolean isEmpty() {
		return size() <= 0;
	}

	@Override
	public void insert(Integer elem) throws IllegalArgumentException {
		if(elem != null){
			list.addSorted(elem);
		}else{
			throw new IllegalArgumentException();
		}
	}

	@Override
	public Integer removeMin() throws NoSuchElementException {
		return list.removeFirst();
	}

	@Override
	public Integer min() throws NoSuchElementException {
		if(!isEmpty()){
			return list.getHead().getElement();
		}else{
			throw new NoSuchElementException();
		}
	}

	@Override
	public Integer[] toArray() {
		return list.toArray();
	}
}
