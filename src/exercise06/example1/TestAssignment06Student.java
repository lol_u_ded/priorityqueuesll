package exercise06.example1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.NoSuchElementException;

import org.junit.jupiter.api.Test;

public class TestAssignment06Student {
	public MyLinkedList list = null;
	
	@Test
	public void testListAddFirst() {
		list = new MyLinkedList();
		
		list.addFirst(Integer.valueOf(2));
		assertEquals(Integer.valueOf(2), list.getFirst());
		list.addFirst(Integer.valueOf(3));
		assertEquals(Integer.valueOf(3), list.getFirst());
		list.addFirst(Integer.valueOf(1));
		assertEquals(Integer.valueOf(1), list.getFirst());
		try { 
			list.addFirst(null); 
		} catch(Exception e) {
			assertTrue(e instanceof IllegalArgumentException);
		}
	}
	
	@Test
	public void testListClear() {
		list = new MyLinkedList();
		
		list.addLast(Integer.valueOf(2));
		list.addFirst(Integer.valueOf(3));
		list.addLast(Integer.valueOf(1));
		list.addFirst(Integer.valueOf(8));
		assertEquals(4, list.size());
		list.clear();
		assertEquals(0, list.size());
		assertTrue(list.getHead()==null);
	}
	
	@Test
	public void testListContains() {
		list = new MyLinkedList();
		
		list.addFirst(Integer.valueOf(2));
		list.addFirst(Integer.valueOf(3));
		list.addFirst(Integer.valueOf(1));
		list.addFirst(Integer.valueOf(8));
		list.addFirst(Integer.valueOf(19));
		list.addFirst(Integer.valueOf(4));
		list.addFirst(Integer.valueOf(3));
		list.addFirst(Integer.valueOf(6));
		assertTrue(list.contains(Integer.valueOf(2)));
		assertTrue(list.contains(Integer.valueOf(3)));
		assertTrue(list.contains(Integer.valueOf(1)));
		assertTrue(list.contains(Integer.valueOf(8)));
		assertTrue(list.contains(Integer.valueOf(19)));
		assertTrue(list.contains(Integer.valueOf(4)));
		assertTrue(list.contains(Integer.valueOf(3)));
		assertTrue(list.contains(Integer.valueOf(6)));
		assertFalse(list.contains(Integer.valueOf(11)));
		assertFalse(list.contains(Integer.valueOf(80)));
		try { 
			list.contains(null); 
		} catch(Exception e) {
			assertTrue(e instanceof IllegalArgumentException);
		}
	}	
	
	// todo: Implement further testcases on your own to test your code.
	
	
	
	/**
	 * ************************************
	 * Test ADT MyListPriorityQueue only
	 * 
	 * ************************************
	 */
	public MyListPriorityQueue pq = new MyListPriorityQueue();
	
	@Test
	public void testPQIsEmpty() {
		assertTrue(pq.isEmpty());
		pq.insert(Integer.valueOf(5));
		assertFalse(pq.isEmpty());
	}
	
	@Test
	public void testPQMin() {
		try { 
			pq.min(); 
		} catch(Exception e) {
			assertTrue(e instanceof NoSuchElementException);
		}
		pq.insert(Integer.valueOf(20));
		assertEquals(Integer.valueOf(20), pq.min());
		pq.insert(Integer.valueOf(21));
		assertEquals(Integer.valueOf(20), pq.min());
		pq.insert(Integer.valueOf(2));
		assertEquals(Integer.valueOf(2), pq.min());
		pq.insert(Integer.valueOf(3));
		assertEquals(Integer.valueOf(2), pq.min());
		pq.insert(Integer.valueOf(1));
		assertEquals(Integer.valueOf(1), pq.min());
	}
	
	@Test
	public void testPQToArray() {
		pq.insert(Integer.valueOf(2));
		pq.insert(Integer.valueOf(3));
		pq.insert(Integer.valueOf(1));
		pq.insert(Integer.valueOf(8));
		pq.insert(Integer.valueOf(19));
		Integer[] a = pq.toArray();
		assertEquals(a.length, pq.size());
		assertEquals(a[0], pq.min());
	}
}